package com.example.entregable_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class Vista3 extends AppCompatActivity {

    private CheckBox chk1;
    private CheckBox chk2;
    private CheckBox chk3;

    private TextView txtCapturaDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista3);

        chk1 = findViewById(R.id.chkFutbol);
        chk2 = findViewById(R.id.chkLeer);
        chk3 = findViewById(R.id.chkEstudiar);

        txtCapturaDatos = findViewById(R.id.capturaCheckbox);
    }

    public void CapturarDatos(View view){
        if (chk1.isChecked() && chk2.isChecked() && chk3.isChecked()){
            txtCapturaDatos.setText("Te gusta jugar " + chk1.getText() + ", " +chk2.getText() + " y " +chk3.getText());
        }else if(chk1.isChecked() && chk2.isChecked()){
            txtCapturaDatos.setText("Te gusta jugar " + chk1.getText() + " y " +chk2.getText());
        }else if(chk1.isChecked() && chk3.isChecked()){
            txtCapturaDatos.setText("Te gusta jugar " + chk1.getText() + " y " +chk3.getText());
        }else if(chk2.isChecked() && chk3.isChecked()){
            txtCapturaDatos.setText("Te gusta " + chk2.getText() + " y " +chk3.getText());
        }else if(chk3.isChecked()){
            txtCapturaDatos.setText("Te gusta " + chk3.getText() + " mucho ");
        }else if(chk2.isChecked()){
            txtCapturaDatos.setText("Te gusta " + chk2.getText() + " muchos libros ");
        }else if(chk1.isChecked()){
            txtCapturaDatos.setText("Te gusta jugar " + chk1.getText());
        }else{
            txtCapturaDatos.setText("Cual es tu hobbie favorito");
        }
    }

    // metodo del boton siguiente
    public void Siguiente(View view){
        Intent siguiente2 = new Intent(this, Vista4.class);
        startActivity(siguiente2);
    }

    // metodo para volver al inicio de sesion
    public void Atras(View view){
        Intent atras2 = new Intent(this, Vista2.class);
        startActivity(atras2);
    }

}