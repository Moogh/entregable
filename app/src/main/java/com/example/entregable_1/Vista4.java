package com.example.entregable_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Vista4 extends AppCompatActivity {

    EditText campo1, campo2;
    TextView resultado;
    int num1, num2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista4);

        campo1 = findViewById(R.id.campo1);
        campo2 = findViewById(R.id.campo2);
        resultado = findViewById(R.id.resultado);
    }

    public void Operacion(View view) {

        num1 = Integer.parseInt(campo1.getText().toString());
        num2 = Integer.parseInt(campo2.getText().toString());

        switch (view.getId()){
            case R.id.botonsumar: sumar();
                break;
            case R.id.botonrestar: restar();
                break;
            case R.id.botonmultiplicar: multiplicar();
                break;
            case R.id.botondividir: dividir();
                break;
        }
    }

    private void sumar(){
        int suma = num1 + num2;
        resultado.setText("El resultado de la suma es " + suma);
    }
    private void restar(){
        int resta = num1 - num2;
        resultado.setText("El resultado de la resta es " + resta);
    }
    private void multiplicar(){
        int multiplicacion = num1 * num2;
        resultado.setText("El resultado de la multiplicacion es " + multiplicacion);
    }
    private void dividir(){
        if(num2 > 0){
            int division = num1 / num2;
            resultado.setText("El resultado de la suma es " + division);
        } else {
            resultado.setText("El numero dos debe ser mayor a cero");
        }
    }

    // metodo del boton hacia atras
    public void Atras3(View view){
        Intent atras3 = new Intent(this, Vista3.class);
        startActivity(atras3);
    }
}