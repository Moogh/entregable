package com.example.entregable_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

public class Vista2 extends AppCompatActivity {
    TextView nombre, apellidos, correo, direccion;
    Button atras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vista2);
        nombre = findViewById(R.id.tvnombre);
        apellidos = findViewById(R.id.tvapellidos);
        correo = findViewById(R.id.tvcorreo);
        direccion = findViewById(R.id.tvdireccion);
        atras = (Button)findViewById(R.id.btnAtras);

        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    protected void onPause(){
        super.onPause();
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean is_wifi = false;
        boolean is_4g = false;
        NetworkInfo nwInfo = cm.getNetworkInfo(cm.getActiveNetwork());
        if(nwInfo.getType() == ConnectivityManager.TYPE_WIFI){
            //Toast.makeText(getApplicationContext(), "Estoy conectado por WiFi", Toast.LENGTH_SHORT).show();
            is_wifi = true;
        }else if (nwInfo.getType() == ConnectivityManager.TYPE_MOBILE){
            is_4g = true;
            //Toast.makeText(this, "Estoy conectado por Datos", Toast.LENGTH_SHORT).show();
        }

        if(is_wifi == false && is_4g==false){
            finish();
        }
    }

    // metodo del boton siguiente
    public void Siguiente(View view){
        Intent siguiente1 = new Intent(this, Vista3.class);
        startActivity(siguiente1);
    }

}